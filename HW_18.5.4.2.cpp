﻿#include <iostream>
#include <string>

using namespace std;

template <typename T>
class Stack
{
private:
    int count = 0;
    T* stack = new T[count];

public:
    Stack()
    {}

    void Push(T el)
    {
        T* pack = new T[count + 1];
        for (int i = 0; i < count; i++)
        {
            pack[i] = stack[i];
        }
        pack[count] = el;
        delete[] stack;
        count++;
        stack = pack;
    }

    void Del()
    {
        T* pack = new T[count - 1];
        for (int i = 0; i < count - 1; i++)
        {
            pack[i] = stack[i];
        }
        delete[] stack;
        count--;
        stack = pack;
    }

    T Pop()
    {
        return stack[count - 1];
    }

    void Print()
    {
        for (int i = 0; i < count; i++)
        {
            cout << *(stack + i) << " ";
        }
        cout << "\n";
    }

    ~Stack()
    {
        delete[] stack;
    }
};


int main()
{
    setlocale(LC_CTYPE, "rus");

    int q;
    int a = 0, i = 0;

    Stack <int> STACK1;

    while (a == 0 || a == 1 || a == 2)
    {
        cout << "Чтобы вывести все элементы стека введите - 0" << "\n";
        cout << "Чтобы добавить элемент введите - 1" << "\n";
        cout << "Чтобы удалить последний элемент введите - 2" << "\n";
        cout << "Чтобы закончить работу программы введите любое другое число" << "\n";
        cin >> a;
        cout << "\n";

        if (a == 0)
        {
            STACK1.Print();
        }
        else if (a == 1)
        {
            STACK1.Push(i);
            q = STACK1.Pop();
            cout << "Элемент " << q << " - добавлен" << "\n";
            i++;
        }
        else if (a == 2)
        {
            STACK1.Del();
            q = STACK1.Pop();
            cout << "Элемент " << q << " - удалён" << "\n";
            i--;
        }
        else (a);
        {
            STACK1.~Stack();
        }
    }
}


